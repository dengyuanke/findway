package com.dengyuanke.findway.dao;


import com.dengyuanke.findway.entity.User;
import com.dengyuanke.findway.util.MyMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author dengyuanke
 * @since 2017-11-14
 */
public interface UserMapper extends MyMapper<User> {

}