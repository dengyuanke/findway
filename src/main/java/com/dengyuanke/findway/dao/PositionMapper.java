package com.dengyuanke.findway.dao;


import com.dengyuanke.findway.entity.Position;
import com.dengyuanke.findway.util.MyMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author dengyuanke
 * @since 2017-11-14
 */
public interface PositionMapper extends MyMapper<Position> {

    @Select("select * from tb_position where phone=#{phone} and (date between #{startDate} and #{endDate})")
    List<Position> getPositions(@Param("phone") String phone, @Param("startDate") String startDate, @Param("endDate") String endDate);
}