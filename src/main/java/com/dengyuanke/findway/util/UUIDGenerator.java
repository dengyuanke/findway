package com.dengyuanke.findway.util;

import java.util.UUID;

/**
 * Created by dengyuanke on 2017/9/28.
 */
public class UUIDGenerator {

    public static String  gen(){
        UUID uuid= UUID.randomUUID();
        String str = uuid.toString();
        String uuidStr=str.replace("-", "");
        return uuidStr;
    }
}
