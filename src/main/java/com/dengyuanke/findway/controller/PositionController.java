package com.dengyuanke.findway.controller;

import com.dengyuanke.findway.dao.PositionMapper;
import com.dengyuanke.findway.dto.JsonResult;
import com.dengyuanke.findway.entity.Position;
import com.dengyuanke.findway.util.UUIDGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class PositionController {

    @Autowired
    private PositionMapper positionMapper;

    /**
     * 插入位置
     * @param position
     * @return
     */
    @PostMapping("/insertposition")
    public JsonResult insertPosition(Position position){
        position.setId(UUIDGenerator.gen());
        position.setDate(new Date());
        int insert = positionMapper.insert(position);
        if (insert>0){
            return JsonResult.renderSuccess("成功");
        }else {
            return JsonResult.renderFail("失败");
        }
    }

    /**
     * 根据日期，获取地址
     * @param phone
     * @return
     */
    @PostMapping("/getpositions")
    public JsonResult getPositions(String phone,String startTime,String endTime){
        List<Position> positions = positionMapper.getPositions(phone, startTime, endTime);
        return JsonResult.renderSuccess(positions);
    }

    @GetMapping("/success")
    public JsonResult success(){
        return JsonResult.renderSuccess();
    }

}
