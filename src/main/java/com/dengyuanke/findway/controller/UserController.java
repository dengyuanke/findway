package com.dengyuanke.findway.controller;

import com.dengyuanke.findway.dao.UserMapper;
import com.dengyuanke.findway.dto.JsonResult;
import com.dengyuanke.findway.entity.User;
import com.dengyuanke.findway.util.UUIDGenerator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.util.StringUtil;

import java.util.Date;

@RestController
public class UserController {

    protected final Logger logger = Logger.getLogger(UserController.class);

    @Autowired
   private UserMapper userMapper;

    /**
     * 获得用户信息，如果用户没有注册，就注册
     * @param phone
     * @return
     */
    @PostMapping("/getuserinfo")
    public JsonResult getUserInfo(String phone){
        if(StringUtil.isEmpty(phone)){
            return JsonResult.renderFail("传入手机号码为空，查询失败");
        }

        User user=new User();
        user.setPhone(phone);
        user = userMapper.selectOne(user);
        if(user!=null){
            return JsonResult.renderSuccess(user);
        }else {
            user=new User();
            user.setPhone(phone);
            user.setId(UUIDGenerator.gen());
            user.setPayed(false);
            user.setCreateTime(new Date());
            userMapper.insert(user);
            return JsonResult.renderSuccess("用户注册成功");
        }
    }

    @PostMapping("/updatepay")
    public JsonResult updatePay(String phone){
        User user=new User();
        user.setPhone(phone);
        user = userMapper.selectOne(user);
        if(user!=null){
            user.setPayed(true);
            int i = userMapper.updateByPrimaryKeySelective(user);
            if(i>0){
                return JsonResult.renderSuccess("支付更新成功");
            }else {
                return JsonResult.renderFail("支付更新失败");
            }

        }else {
            return JsonResult.renderFail("未查询到该用户");
        }
    }

}
