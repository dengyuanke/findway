package com.dengyuanke.findway.dto;


public class JsonResult {
    private boolean success;
    private String status;
    private String msg;
    private Object obj;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    /**
     * 渲染出错数据
     *
     * @return result
     */
    public static JsonResult renderError() {
        JsonResult result = new JsonResult();
        result.setSuccess(false);
        result.setStatus("500");
        return result;
    }

    /**
     * 渲染出错数据（带消息）
     *
     * @param msg 需要返回的消息
     * @return result
     */
    public static JsonResult renderError(String msg) {
        JsonResult result = renderError();
        result.setMsg(msg);
        return result;
    }

    /**
     * 渲染成功数据
     *
     * @return result
     */
    public static JsonResult renderSuccess() {
        JsonResult result = new JsonResult();
        result.setSuccess(true);
        result.setStatus("200");
        return result;
    }

    /**
     * 渲染成功数据（带信息）
     *
     * @param msg 需要返回的信息
     * @return result
     */
    public static JsonResult renderSuccess(String msg) {
        JsonResult result = renderSuccess();
        result.setMsg(msg);
        return result;
    }

    /**
     * 渲染成功数据（带数据）
     *
     * @param obj 需要返回的对象
     * @return result
     */
    public static JsonResult renderSuccess(Object obj) {
        JsonResult result = renderSuccess();
        result.setObj(obj);
        return result;
    }

    /**
     * 渲染失败数据
     *
     * @return result
     */
    public static JsonResult renderFail() {
        JsonResult result = new JsonResult();
        result.setSuccess(false);
        result.setStatus("400");
        return result;
    }

    /**
     * 渲染失败数据（带信息）
     *
     * @param msg 需要返回的信息
     * @return result
     */
    public static JsonResult renderFail(String msg) {
        JsonResult result = renderFail();
        result.setMsg(msg);
        return result;
    }

    /**
     * 渲染失败数据（带数据）
     *
     * @param obj 需要返回的对象
     * @return result
     */
    public static JsonResult renderFail(Object obj) {
        JsonResult result = renderFail();
        result.setObj(obj);
        return result;
    }



}
