package com.dengyuanke.findway.service;

import com.dengyuanke.findway.entity.User;

public interface UserService {
    User findUserById(String id);

    User findUserByUserName(String username);
}
